import React from 'react';
import './App.css';

import Header from '../src/components/Header/header';
import Principal from '../src/objects/main/main';
import Section2 from './components/Section2/section2';
import Form from './components/Form/form';

function App() {
  return (
    <>
    <Header />
    <Principal />
    <Section2 />
    <Form />
    </>
  );
}

export default App;

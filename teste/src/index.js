import React from 'react';
import ReactDOM from 'react-dom';

import './styles/generic/reset.css';
import './styles/settings/settings.css';


import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

import React from 'react';
import "../content/content.css";
import Grant from '../../Imgs/Grant.png';
import Red from '../../Imgs/Red.png';
import Sybil from '../../Imgs/Sybil.png';

const Content = () => (
    <>
       <div className="quadrado_externo">
           <div className="quadrado">
                <img className="chars" src= {Grant} alt="Personagem Grant"/> 
                <p className= "char_text">A Camerata foi apenas os dois no inicio, e suas fileiras nunca foram destinadas a exceder um número a ser contado em uma mão.</p>
            </div>
        </div>
       <div className="quadrado_externo">
           <div className="quadrado">
                <img className="chars" src= {Red} alt="Personagem Red"/>
                <p className= "char_text">Red, uma jovem cantora, entrou em posse do Transistor.Sendo a poderosa espada falante.O grupo Possessores quer tanto ela quanto o Transistor e está perseguindo implacavelmente a sua procura.</p>
           </div>
        </div>
       <div className="quadrado_externo">
           <div className="quadrado">
                <img className="chars" src= {Sybil} alt="Personagem Sybil"/>
                <p className= "char_text">Sybil é descrita pelo Transistor como sendo os "Os ouvidos" da Camerata.</p>
           </div>
        </div>
    </>
);

export default Content;
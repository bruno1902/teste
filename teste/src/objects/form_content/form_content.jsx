import React from 'react';
import '../../objects/form_content/form_content.css';

const Content = () => (
    <div className="section_form">
        <h1 className="form_title">Formulario</h1>
        <p className="form_subtitle" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quisquam aliquam dolor impedit eaque veritatis numquam, repellat ullam autem voluptates accusamus beatae assumenda alias aut exercitationem tempore ab enim delectus?</p>
        <form className="form_dates">
            <input className="form_name" type="text" name="name" placeholder="Nome" required />
            <input className="form_email" type="email" name="email" placeholder="Email" required />
        </form>
        <form className="form_submit">
             <textarea className="form_textarea" placeholder="Mensagem"></textarea>
             <input className="submit" type="submit"></input>
        </form>
    </div> 
);
export default Content;
import React from 'react';
import '../../objects/main/main.css';
import bgc from '../../Imgs/background.png';
import bgc_sprite from '../../Imgs/Gradiente.svg';
import card_principal from '../../Imgs/imagem_principal.png';
import feather1 from '../../Imgs/pena1.png';
import feather2 from '../../Imgs/pena2.png';
import feather3 from '../../Imgs/pena3.png';
import feather4 from '../../Imgs/pena4.png';

const principal = () => (
    <>
    <img className="principal_bgc" src={bgc} alt="Imagem de fundo"/>
    <img className="card_principal" src={card_principal} alt="Card principal" srcset=""/>
    <img className="card_sprite" src={bgc_sprite} alt="" srcset=""/>
    <img className="card_feather" src= {feather1} alt="" srcset=""/>
    <img className="card_feather" src= {feather2} alt="" srcset=""/>
    <img className="card_feather" src= {feather3} alt="" srcset=""/>
    <img className="card_feather" src= {feather4} alt="" srcset=""/>
    <div className="card_principal_text">
    <p>"Olha, o que quer que você</p>
    <p>esteja pensando, me faça</p>
    <p>um favor, não solte."</p>
    </div>
    </>
);

export default principal;
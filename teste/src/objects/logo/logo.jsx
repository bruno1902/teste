import React from 'react';
import img_logo from '../../Imgs/espada.png';
import '../../objects/logo/logo.css';

const Logo = () => (
    <>
        <img src={img_logo} alt="espada" srcset=""/>
        <h1 className="text_logo">supergiantgames</h1>
    </>
);

export default Logo;
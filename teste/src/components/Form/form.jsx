import React from 'react';
import '../Form/form.css';
import Content from '../../objects/form_content/form_content.jsx'

const Form = () => (
    <div className="form_section">
        <Content />
    </div>
);

export default Form;
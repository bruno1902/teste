import React from 'react';
import '../../components/Header/header.css';
import Logo from '../../objects/logo/logo';

const Header = () => <div className="Container_header"><Logo /></div>

export default Header;
import React from 'react';
import '../../components/Section2/section2.css';

import Content from '../../objects/content/content'

const Section2 = () => (
    <>
        <div className="section2_container">
            <Content />
        </div> 
    </>
);

export default Section2;